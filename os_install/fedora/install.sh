
# By: Jasti Sri Radhe Shyam

# Tilix
sudo dnf install tilix -y

# ZSH
sudo dnf install zsh -y
# Oh my ZSH
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
